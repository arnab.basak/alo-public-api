"use strict";

const uuid = require("uuid");
const log = require("./log");
const Dao = require("./dao");
const { SCHEMA, TABLE } = require("./constant");

const save_sms_request_log = async (request, data) => {
    try {
        const sql = {
            text: `insert into ${SCHEMA.PUBLIC}.${TABLE.SMS_LOG} (oid, mobileNo, sms, templateId, referenceOid, referenceType, companyOid, masking) 
                values ($1, $2, $3, $4, $5, $6, $7, $8)`,
            values: [
                uuid.v4(),
                data.mobileNo,
                data.sms_template,
                data.templateId ? data.templateId : null,
                data.referenceOid ? data.referenceOid : 'ref-oid',
                data.referenceType ? data.referenceType : 'ref-type',
                data.companyOid ? data.companyOid : null,
                data.masking ? data.masking : 'IDRA'
            ],
        };
        await Dao.execute_value(request.pg, sql);
    } catch (e) {
        log.error(`An exception occurred while save smslog : ${e?.message}`);
    }
};

const get_company_masking = async (request, companyOid) => {
    let data = null;
    let sql = {
        text: `select c.companyjson::json ->> 'masking' as masking
            from ${SCHEMA.PUBLIC}.${TABLE.COMPANY} c
            where 1 = 1 and c.oid = $1`,
        values: [companyOid],
    };
    try {
        let data_set = await Dao.get_data(request.pg, sql);
        data = data_set[0]['masking'];
    } catch (e) {
        log.error(`An exception occurred while getting company masking : ${e?.message}`);
    }
    return data;
};

const get_message_template = async (request, templateId) => {
    let data = null;
    let sql = {
        text: `select m.templatetext as "templateText"
            from ${SCHEMA.PUBLIC}.${TABLE.MESSAGE_TEMPLATE} m
            where 1 = 1 and m.templateid = $1 and m.status = $2`,
        values: [templateId, 'Active'],
    };
    try {
        let data_set = await Dao.get_data(request.pg, sql);
        data = data_set[0]['templateText'];
    } catch (e) {
        log.error(`An exception occurred while getting message template : ${e?.message}`);
    }
    return data;
};

module.exports = {
    save_sms_request_log: save_sms_request_log,
    get_company_masking: get_company_masking,
    get_message_template: get_message_template,
};
