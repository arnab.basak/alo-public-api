"use strict";

module.exports = {
    BEARER: "Bearer",
    ALGORITHM: "HS256",
    TABLE: {
        SMS_LOG: "SmsLog",
        SMS_REPORT: "SmsReport",
        SMS_SUCCESS_LOG: "SmsLogSuccess",
        SMS_FAILED_LOG: "SmsLogFailed",

        LOGIN: "Login",
        COMPANY: "Company",
        ROLE: "Role",
        ACCESS_NUMBER: "AccessNumber",
        LOGIN_LOG: "LoginLog",
        MESSAGE_TEMPLATE: "MessageTemplate",
        MESSAGE_PROVIDER: "MessageProvider",

        FORM_TYPE: "FormType",
        BATCH: "Batch",
        GEO_DATA: "GeoData",

        BANK: "Bank",
        BRANCH: "Branch",

        REJECTION_REASON: "RejectionReason",

        //life

        LIFE_AGENT_APPLI: "LifeAgentAppli",
        LIFE_AGENT: "LifeAgent",
        LIFE_TRAIN_HIST: "LifeTrainHist",
        TRAINING_INSTITUTE: "TrainingInstitute",

        LIFE_TMP_AGENT_APPLICATION: "LifeTmpAgentAppli",
        LIFE_TMP_EMPLOYEE_LETTER: "LifeTmpEmployLet",

        LIFE_AGENT_LICENCE: "LifeAgentLicence",
        LIFE_AGENT_REGISTER: "LifeAgentRegister",

        LIFE_RELEASE_LETTER: "LifeReleaseLetter",
        LIFE_AGENT_REGISTER_SUMMERY: "LifeAgentRegisterSummery",

        //non-life

        NON_LIFE_AGENT_APPLI: "NonLifeAgentAppli",
        NON_LIFE_AGENT: "NonLifeAgent",
        NON_LIFE_TRAIN_HIST: "NonLifeTrainHist",

        NON_LIFE_TMP_AGENT_APPLICATION: "NonLifeTmpAgentAppli",
        NON_LIFE_TMP_EMPLOYEE_LETTER: "NonLifeTmpEmployLet",

        NON_LIFE_AGENT_LICENCE: "NonLifeAgentLicence",
        NON_LIFE_AGENT_REGISTER: "NonLifeAgentRegister",

        NON_LIFE_RELEASE_LETTER: "NonLifeReleaseLetter",
        NOTIFICATION: "Notification",
        NOTIFICATION_TEMPLATE: "NotificationTemplate",
        NON_LIFE_AGENT_REGISTER_SUMMERY: "NonLifeAgentRegisterSummery",

    },
    SCHEMA: {
        PUBLIC: "public",
    },
    TEXT: {
        FORM_ONE: 1,
        FORM_FOUR: 4,
        FORM_SIX: 6
    },
    API: {

        CONTEXT: "/api",
        STATUS: "/monitor/status",
        HEALTH_CHECK: "/monitor/health-check",

        SIGNIN: "/v1/user/signin",
        SIGNOUT: "/v1/user/signout"

    },

};
