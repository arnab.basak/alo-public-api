"use strict";

const chai = require("chai");
const { get_token, revoke_token } = require("../authenticate");
const chaiHttp = require("chai-http");
chai.use(chaiHttp);
const { server } = require("../../src/server");
let should = require('chai').should();
const { API, BEARER, ROLE } = require("../config");

describe('Should verify Access Number', () => {

    var access_token;

    before((done) => {
        get_token(ROLE.PGL_REV_01).then(token => {
            access_token = token;
            done();
        });
    });

    after(function (done) {
        revoke_token(access_token).then((data) => {
            console.log('End Test');
            done();
        });
    });

    it('It should verify access number', (done) => {
        chai.request(API.BASE_URL)
            .post(API.CONTEXT + API.AGENT_ACCESS_NUMBER_VERY_PATH)
            .set('Authorization', `${BEARER} ${access_token}`)
            .send({
                "accessNumber": "037451"
            })
            .end((err, res) => {
                if (err) { return false; }
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('status');
                res.body.should.have.property('code');
                res.body.should.have.property('data').be.a('object');
                res.body.should.have.property('status').eql(true);
                res.body.should.have.property('code').eql(200);
                res.body.should.have.nested.property('data.accessNumber');
                res.body.should.have.nested.property('data.accessNumber').eql("037451");
                res.body.should.have.nested.property('data.name');
                res.body.should.have.nested.property('data.companyOid');
                res.body.should.have.nested.property('data.mobileNo');
                done();
            });
    });
    
    it('It should verify access number', (done) => {
        chai.request(API.BASE_URL)
            .post(API.CONTEXT + API.AGENT_ACCESS_NUMBER_VERY_PATH)
            .set('Authorization', `${BEARER} ${access_token}`)
            .send({
                "accessNumber": "037452"
            })
            .end((err, res) => {
                if (err) { return false; }
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('status');
                res.body.should.have.property('code');
                res.body.should.have.property('status').eql(false);
                res.body.should.have.property('code').eql(201);
                res.body.should.have.nested.property('message').eql("এক্সেস নম্বর টি পাওয়া যায়নি");
                done();
            });
    });
});
