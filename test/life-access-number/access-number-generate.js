"use strict";

const chai = require("chai");
const chaiHttp = require("chai-http");
const { get_token, revoke_token } = require("../authenticate");
const { server } = require("../../src/server");
const { API, BEARER, ROLE } = require("../config");
let should = require('chai').should();
chai.use(chaiHttp);

describe('Should generate Access Number', () => {

    var access_token;
    
    before((done) => {
        get_token(ROLE.PGL_REV_01).then(token => {
            access_token = token;
            done();
        });
    });

    after(function (done) {
        revoke_token(access_token).then((data) => {
            done();
        });
    });


    it('It should generate access number', (done) => {
        chai.request(API.BASE_URL)
            .post(API.CONTEXT + API.AGENT_ACCESS_NUMBER_GEN_PATH)
            .set('Authorization', `${BEARER} ${access_token}`)
            .send({
                "name": "Rasel",
                "mobileNo": "01621460693"
            })
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('status');
                res.body.should.have.property('code');
                res.body.should.have.property('data').be.a('object');
                res.body.should.have.property('status').eql(true);
                res.body.should.have.property('code').eql(200);
                res.body.should.have.nested.property('data.accessNumber');
                res.body.should.have.nested.property('data.name').eql("Rasel");
                res.body.should.have.nested.property('data.mobileNo').eql("01621460693");
                done();
            });
    });

    it('It should generate access number', (done) => {
        chai.request(API.BASE_URL)
            .post(API.CONTEXT + API.AGENT_ACCESS_NUMBER_GEN_PATH)
            .set('Authorization', `${BEARER} ${access_token}`)
            .send({
                "name": "Rasel",
                "mobileNo": "01621460693"
            })
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('status');
                res.body.should.have.property('code');
                res.body.should.have.property('data').be.a('object');
                res.body.should.have.property('status').eql(true);
                res.body.should.have.property('code').eql(200);
                res.body.should.have.nested.property('data.accessNumber');
                res.body.should.have.nested.property('data.name').eql("Rasel");
                res.body.should.have.nested.property('data.mobileNo').eql("01621460693");
                done();
            });
    });

    it('should not generate access number for wrong phone number', (done) => {
        chai.request(API.BASE_URL)
            .post(API.CONTEXT + API.AGENT_ACCESS_NUMBER_GEN_PATH)
            .set('Authorization', `${BEARER} ${access_token}`)
            .send({
                "name": "Rasel",
                "mobileNo": "0162146069"
            })
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('status');
                res.body.should.have.property('code');
                res.body.should.have.property('status').eql(false);
                res.body.should.have.property('code').eql(301);
                done();
            });
    });
});


