"use strict";

const chai = require("chai");
const chaiHttp = require("chai-http");
const { server } = require("../../src/server");
const { API, BEARER, ROLE } = require("../config");
let should = require('chai').should();
chai.use(chaiHttp);



describe('Should delete Public file', () => {

    before((done) => {
        console.log("Testing Start");
        done();
    });

    after(function (done) {
        console.log("Testing end");
        done();
    });

    it('It should delete Public file by companyOid and fileName', (done) => {
        chai.request(API.BASE_URL)
            .delete(API.CONTEXT + API.AGENT_APPLICATION_DELETE_PUBLIC_PATH + '/pragatilife/2fe85136-bdad-4d13-8822-134a7c78407b.png')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('status');
                res.body.should.have.property('code');
                done();
            });
        });
});

//NODE_ENV=loc node_modules/mocha/bin/mocha 'test/public/public-delete-file.js' --timeout 1000 --reporter mochawesome --reporter-options reportDir=test_report,reportFilename=test_report_$(date +%Y%m%d_%H%M%S),json=false --exit