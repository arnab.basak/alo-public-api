"use strict";

const chai = require("chai");
const chaiHttp = require("chai-http");
const { server } = require("../../src/server");
const { API, BEARER, ROLE } = require("../config");
let should = require('chai').should();
chai.use(chaiHttp);

describe('Should download public file', () => {

    it('It should download public file by companyOid and fileName', (done) => {
        chai.request(API.BASE_URL)
            .get(API.CONTEXT + API.AGENT_APPLICATION_DOWNLOAD_PUBLIC_PATH + '/pragatilife/0f373b8f-aab8-4b78-aea2-0622821ad240.png')
            .end((err, res) => {
                // res.should.have.status(200);
                done();
            });
        });
});

//NODE_ENV=loc node_modules/mocha/bin/mocha 'test/public/public-download-file.js' --timeout 1000 --reporter mochawesome --reporter-options reportDir=test_report,reportFilename=test_report_$(date +%Y%m%d_%H%M%S),json=false --exit