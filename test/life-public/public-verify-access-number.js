"use strict";

const chai = require("chai");
const chaiHttp = require("chai-http");
const { server } = require("../../src/server");
const { API, BEARER, ROLE } = require("../config");
let should = require('chai').should();
chai.use(chaiHttp);

describe('Should verify public access number', () => {

    before((done) => {
        console.log("Testing Start");
        done();
    });

    after(function (done) {
        console.log("Testing end");
        done();
    });

    it('It should verify public access number', (done) => {
        chai.request(API.BASE_URL)
            .post(API.CONTEXT + API.AGENT_ACCESS_NUMBER_VERY_PUBLIC_PATH)
            .send({
                "accessNumber": "037451"
            })
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('status');
                res.body.should.have.property('code');
                res.body.should.have.property('data').be.a('object');
                res.body.should.have.property('status').eql(true);
                res.body.should.have.property('code').eql(200);
                res.body.should.have.nested.property('data.accessNumber');
                res.body.should.have.nested.property('data.accessNumber').eql("037451");
                res.body.should.have.nested.property('data.name');
                res.body.should.have.nested.property('data.companyOid');
                res.body.should.have.nested.property('data.mobileNo');
                done();
            });
    });
});

//NODE_ENV=loc node_modules/mocha/bin/mocha 'test/public/public-verify-access-number.js' --timeout 1000 --reporter mochawesome --reporter-options reportDir=test_report,reportFilename=test_report_$(date +%Y%m%d_%H%M%S),json=false --exit