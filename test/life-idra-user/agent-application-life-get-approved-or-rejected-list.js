"use strict";

const chai = require("chai");
const chaiHttp = require("chai-http");
const { get_token, revoke_token } = require("../authenticate");
const { server } = require("../../src/server");
const { API, BEARER, ROLE } = require("../config");
let should = require('chai').should();
chai.use(chaiHttp);

describe('Should get agent application approved or rejected licence list', () => {
    var access_token;

    before((done) => {
        get_token(ROLE.IDRA_APP_01).then(token => {
            access_token = token;
            done();
        });
    });

    after(function (done) {
        revoke_token(access_token).then((data) => {
            done();
        });
    });

    it('It should get agent application approved or rejected licence list', (done) => {
        chai.request(API.BASE_URL)
            .get(API.CONTEXT + API.LIFE_AGENT_APPLICATION_GET_APPROVED_OR_REJECTED_LIST + '?offset=0&limit=10&sortColId=createdOn&sortOrder=desc&searchText=&fromDate=&toDate=&companyOid=&formTypeOid=&companyType=&status=Approved')
            .set('Authorization', `${BEARER} ${access_token}`)
            .send()
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('status');
                res.body.should.have.property('code');
                res.body.should.have.property('data').be.a('array');
                res.body.should.have.property('status').eql(true);
                res.body.should.have.property('code').eql(200);
                res.body.should.have.nested.property('data[0].lifeAgentLicenceOid');
                res.body.should.have.nested.property('data[0].applicationId');
                res.body.should.have.nested.property('data[0].name');
                res.body.should.have.nested.property('data[0].licenceNumber');
                res.body.should.have.nested.property('data[0].companyOid');
                res.body.should.have.nested.property('data[0].formTypeOid');
                res.body.should.have.nested.property('data[0].formTypeNameEn');
                res.body.should.have.nested.property('data[0].bankName');
                res.body.should.have.nested.property('data[0].bankAccountNo');
                res.body.should.have.nested.property('data[0].status');
                res.body.should.have.nested.property('data[0].batchId');
                done();
            });
    });
});

//NODE_ENV=loc node_modules/mocha/bin/mocha 'test/idra-user/agent-application-life-get-approved-or-rejected-list.js' --timeout 1000 --reporter mochawesome --reporter-options reportDir=test_report,reportFilename=test_report_$(date +%Y%m%d_%H%M%S),json=false --exit