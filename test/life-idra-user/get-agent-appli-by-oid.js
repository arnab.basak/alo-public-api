"use strict";

const chai = require("chai");
const chaiHttp = require("chai-http");
const { get_token, revoke_token } = require("../authenticate");
const { server } = require("../../src/server");
const { API, BEARER, ROLE } = require("../config");
let should = require('chai').should();
chai.use(chaiHttp);

describe('Should get agent application', () => {
    var access_token;

    before((done) => {
        get_token(ROLE.IDRA_APP_01).then(token => {
            access_token = token;
            done();
        });
    });

    after(function (done) {
        revoke_token(access_token).then((data) => {
            done();
        });
    });

    it('It should get agent application by OID', (done) => {
        chai.request(API.BASE_URL)
            .get(API.CONTEXT + API.AGENT_APPLICATION_GET_BY_OID_PATH + '/4a413e15-e7a8-41c1-9678-30b1cca3443d')
            .set('Authorization', `${BEARER} ${access_token}`)
            .send()
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('status');
                res.body.should.have.property('code');
                res.body.should.have.property('data').be.a('object');
                res.body.should.have.property('status').eql(true);
                res.body.should.have.property('code').eql(200);
                res.body.should.have.nested.property('data.agentCode');
                res.body.should.have.nested.property('data.applicationId');
                res.body.should.have.nested.property('data.bankAccountNo');
                res.body.should.have.nested.property('data.bankName');
                res.body.should.have.nested.property('data.eduQualification');
                res.body.should.have.nested.property('data.oid');
                res.body.should.have.nested.property('data.companyOid');
                res.body.should.have.nested.property('data.nameBn');
                res.body.should.have.nested.property('data.nidNumber');
                res.body.should.have.nested.property('data.resCountry');
                res.body.should.have.nested.property('data.religion');
                done();
            });
    });
});

//NODE_ENV=loc node_modules/mocha/bin/mocha 'test/idra-user/get-agent-appli-by-oid.js' --timeout 1000 --reporter mochawesome --reporter-options reportDir=test_report,reportFilename=test_report_$(date +%Y%m%d_%H%M%S),json=false --exit