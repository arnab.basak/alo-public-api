"use strict";

const chai = require("chai");
const chaiHttp = require("chai-http");
const { get_token, revoke_token } = require("../authenticate");
const { server } = require("../../src/server");
const { API, BEARER, ROLE } = require("../config");
let should = require('chai').should();
chai.use(chaiHttp);

describe('Should approve agent Application renew licence', () => {
    var access_token;

    before((done) => {
        get_token(ROLE.IDRA_APP_01).then(token => {
            access_token = token;
            done();
        });
    });

    after(function (done) {
        revoke_token(access_token).then((data) => {
            done();
        });
    });

    it('It should approve agent Application renew licence', (done) => {
        chai.request(API.BASE_URL)
            .post(API.CONTEXT + API.AGENT_APPLICATION_RENEW_LICENCE_APPROVE_PATH)
            .set('Authorization', `${BEARER} ${access_token}`)
            .send({
                "oid": "r5929a76-2834-4989-9100-89d1dda1e79c",
                "status": "Approved",
                "rejectionCause": ""
            })
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('status');
                res.body.should.have.property('code');
                res.body.should.have.property('status').eql(true);
                res.body.should.have.property('code').eql(200);
                res.body.should.have.property('licenceNumber');
                res.body.should.have.property('message').eql("Successfully application approved");
                done();
            });
    });
});

//NODE_ENV=loc node_modules/mocha/bin/mocha 'test/idra-user/agent-application-renew-licence-approve.js' --timeout 1000 --reporter mochawesome --reporter-options reportDir=test_report,reportFilename=test_report_$(date +%Y%m%d_%H%M%S),json=false --exit