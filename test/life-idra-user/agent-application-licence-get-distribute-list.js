"use strict";

const chai = require("chai");
const chaiHttp = require("chai-http");
const { get_token, revoke_token } = require("../authenticate");
const { server } = require("../../src/server");
const { API, BEARER, ROLE } = require("../config");
let should = require('chai').should();
chai.use(chaiHttp);

describe('Should get agent application licence distribute list', () => {
    var access_token;

    before((done) => {
        get_token(ROLE.IDRA_APP_01).then(token => {
            access_token = token;
            done();
        });
    });

    after(function (done) {
        revoke_token(access_token).then((data) => {
            done();
        });
    });

    it('It should get agent application licence distribute list', (done) => {
        chai.request(API.BASE_URL)
            .get(API.CONTEXT + API.AGENT_APPLICATION_LICENCE_GET_DISTRIBUTE_LIST_PATH + '?offset=0&limit=10&sortColId=createdOn&sortOrder=desc&fromDate=&toDate=&companyOid=&searchText=&status=Active')
            .set('Authorization', `${BEARER} ${access_token}`)
            .send()
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('status');
                res.body.should.have.property('code');
                res.body.should.have.property('data').be.a('array');
                res.body.should.have.property('status').eql(true);
                res.body.should.have.property('code').eql(200);
                res.body.should.have.nested.property('data[0].oid');
                res.body.should.have.nested.property('data[0].agentCode');
                res.body.should.have.nested.property('data[0].licenceNumber');
                res.body.should.have.nested.property('data[0].salutation');
                res.body.should.have.nested.property('data[0].nameEn');
                res.body.should.have.nested.property('data[0].status');
                done();
            });
    });
});


//NODE_ENV=loc node_modules/mocha/bin/mocha 'test/idra-user/agent-application-licence-get-distribute-list.js' --timeout 1000 --reporter mochawesome --reporter-options reportDir=test_report,reportFilename=test_report_$(date +%Y%m%d_%H%M%S),json=false --exit