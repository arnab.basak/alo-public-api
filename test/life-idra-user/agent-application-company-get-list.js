"use strict";

const chai = require("chai");
const chaiHttp = require("chai-http");
const { get_token, revoke_token } = require("../authenticate");
const { server } = require("../../src/server");
const { API, BEARER, ROLE } = require("../config");
let should = require('chai').should();
chai.use(chaiHttp);

describe('Should get company list', () => {
    var access_token;

    before((done) => {
        get_token(ROLE.IDRA_APP_01).then(token => {
            access_token = token;
            done();
        });
    });

    after(function (done) {
        revoke_token(access_token).then((data) => {
            done();
        });
    });

    it('It should get company list', (done) => {
        chai.request(API.BASE_URL)
            .get(API.CONTEXT + API.AGENT_APPLICATION_COMPANY_RESOURCES + '?companyType=Life&status=Active')
            .set('Authorization', `${BEARER} ${access_token}`)
            .send()
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('status');
                res.body.should.have.property('code');
                res.body.should.have.property('data').be.a('array');
                res.body.should.have.property('status').eql(true);
                res.body.should.have.property('code').eql(200);
                res.body.should.have.nested.property('data[0].oid');
                res.body.should.have.nested.property('data[0].nameBn');
                res.body.should.have.nested.property('data[0].companyId');
                res.body.should.have.nested.property('data[0].prefix');
                res.body.should.have.nested.property('data[0].mnemonic');
                res.body.should.have.nested.property('data[0].name');
                done();
            });
    });
});

//NODE_ENV=loc node_modules/mocha/bin/mocha 'test/idra-user/agent-application-company-get-list.js' --timeout 1000 --reporter mochawesome --reporter-options reportDir=test_report,reportFilename=test_report_$(date +%Y%m%d_%H%M%S),json=false --exit