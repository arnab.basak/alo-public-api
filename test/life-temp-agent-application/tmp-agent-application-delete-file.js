"use strict";

const chai = require("chai");
const chaiHttp = require("chai-http");
const { get_token, revoke_token } = require("../authenticate");
const { server } = require("../../src/server");
const { API, BEARER, ROLE } = require("../config");
let should = require('chai').should();
chai.use(chaiHttp);

describe('Should delete life tmp agent application file', () => {
    var access_token;

    before((done) => {
        get_token(ROLE.PGL_REV_01).then(token => {
            access_token = token;
            done();
        });
    });

    after(function (done) {
        revoke_token(access_token).then((data) => {
            done();
        });
    });

    it('It should delete life tmp agent application file by companyOid and fileName', (done) => {
        chai.request(API.BASE_URL)
            .delete(API.CONTEXT + API.AGENT_APPLICATION_DELETE_PATH + '/pragatilife/e7f0202e-c283-4e5d-b246-681de675fd4d.png')
            .set('Authorization', `${BEARER} ${access_token}`)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('status');
                res.body.should.have.property('code');
                done();
            });
    });
});

//NODE_ENV=loc node_modules/mocha/bin/mocha 'test/tmp-agent-application/tmp-agent-application-delete-file.js' --timeout 1000 --reporter mochawesome --reporter-options reportDir=test_report,reportFilename=test_report_$(date +%Y%m%d_%H%M%S),json=false --exit