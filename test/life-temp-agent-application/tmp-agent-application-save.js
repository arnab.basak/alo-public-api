"use strict";

const chai = require("chai");
const chaiHttp = require("chai-http");
const { get_token, revoke_token } = require("../authenticate");
const { server } = require("../../src/server");
let should = require('chai').should();
const { API, BEARER, ROLE } = require("../config");
chai.use(chaiHttp);

describe('Should save temp agent application', () => {
    
    var access_token;

    before((done) => {
        get_token(ROLE.PGL_REV_01).then(token => {
            access_token = token;
            done();
        });
    });

    after(function (done) {
        revoke_token(access_token).then((data) => {
            done();
        });
    });

    it('It should save temp agent application', (done) => {
        chai.request(API.BASE_URL)
            .post(API.CONTEXT + API.AGENT_APPLICATION_TMP_SAVE)
            .set('Authorization', `${BEARER} ${access_token}`)
            .send({
                "resCountry": "Bangladesh",
                "perCountry": "Bangladesh",
                "otherImagePath": [
                  "fc89b2ed-7045-448c-b3fe-188c6d91c1a1.png",
                  "8151ab2a-b881-4e97-a251-3df52f77a1a6.png"
                ],
                "nonOfficerDeclare": "Y",
                "nonQuaSub08": "Y",
                "nonViolateSub09": "Y",
                "nationality": "বাংলাদেশী",
                "mobileNo": "01621460693",
                "nameEn": "MAINUR RASEL",
                "religion": "Islam",
                "nameBn": "মাইনুর রাসেল",
                "fatherName": "ABDUL MAZID",
                "motherName": "RAHELA MAZID",
                "dateOfBirth": "1996-12-31",
                "resAddressLine1": "DG-5052,khilkhet,Dhaka",
                "resPostOffice": "khilkhet",
                "resPostalCode": "1229",
                "resDistrict": "26",
                "resUpazila": "37",
                "resUnion": "17",
                "resDivision": "30",
                "perAddressLine1": "DG-5052,khilkhet,Dhaka",
                "perPostOffice": "khilkhet",
                "perPostalCode": "1229",
                "perDivision": "30",
                "perDistrict": "26",
                "perUpazila": "37",
                "perUnion": "17",
                "trainingName": "N/A",
                "bankName": "DBBL",
                "branchName": "UTTARA",
                "bankAccountNo": "1221",
                "eduQualification": "Bachelor",
                "eduCertificatePath": "28451f8f-ad6a-4074-9eea-19905f2e23b2.png",
                "nidNumber": "1212323432",
                "nidImagePathFront": "c118ee93-44a3-4ccf-a231-c910d8385de0.png",
                "nidImagePathBack": "8096f891-b69b-43d8-ad78-9448e3044926.png",
                "birthCertOrPassNo": "1211",
                "birthCertOrPassPath": "84a2fcf6-4003-478f-8089-e6349d7a12a0.png",
                "preInsrEmployNo": "1211",
                "preInsrEmployExpDate": "2021-11-04",
                "preInsrLicenceNo": "121",
                "preInsrLicenceExpDate": "2021-11-07",
                "preInsReleaseLetterNo": "121",
                "preInsReleaseLetterDate": "2021-11-16",
                "email": "mainur@gmail.com",
                "gender": "Male",
                "tin": "232233223323",
                "agentPhotoPath": "0aa2a05a-334a-4e57-a8b4-fca725ff09f7.png",
                "agentSignaturePath": "bbfc5c96-fe1b-432b-815d-604f38867b72.png",
                "accessNumber": "057839",
                "birthOrPassport": "BirthCertificate",
                "appliMode": "InOffice"
              })
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('status');
                res.body.should.have.property('code');
                res.body.should.have.property('status').eql(true);
                res.body.should.have.property('code').eql(200);
                res.body.should.have.property('message').eql("এজেন্ট আবেদনপত্র সফলভাবে জমা দেওয়া হয়েছে");
                done();
            });
    });
});

//NODE_ENV=loc node_modules/mocha/bin/mocha 'test/tmp-agent-application/tmp-agent-application-save.js' --timeout 1000 --reporter mochawesome --reporter-options reportDir=test_report,reportFilename=test_report_$(date +%Y%m%d_%H%M%S),json=false --exit
