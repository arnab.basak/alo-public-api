"use strict";

const chai = require("chai");
const chaiHttp = require("chai-http");
const { get_token, revoke_token } = require("../authenticate");
const { server } = require("../../src/server");
const { API, BEARER, ROLE } = require("../config");
let should = require('chai').should();
chai.use(chaiHttp);

describe('Should lock temp agent application', () => {
    var access_token;

    before((done) => {
        get_token(ROLE.PGL_REV_01).then(token => {
            access_token = token;
            done();
        });
    });

    after(function (done) {
        revoke_token(access_token).then((data) => {
            done();
        });
    });

    it('It should lock temp agent application', (done) => {
        chai.request(API.BASE_URL)
            .post(API.CONTEXT + API.AGENT_APPLICATION_TMP_LOCK)
            .set('Authorization', `${BEARER} ${access_token}`)
            .send({
                "requestData": [
                    {
                        "oid": "56b62741-9ef9-458e-a6ec-9aa63174b773"
                    }
                ]
            })
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('status');
                res.body.should.have.property('code');
                res.body.should.have.property('status').eql(true);
                res.body.should.have.property('code').eql(200);
                res.body.should.have.property('message').eql("আবেদন টি সফলভাবে লক করা হয়েছে");
                done();
            });
    });

    
});

//NODE_ENV=loc node_modules/mocha/bin/mocha 'test/tmp-agent-application/tmp-agent-application-lock.js' --timeout 1000 --reporter mochawesome --reporter-options reportDir=test_report,reportFilename=test_report_$(date +%Y%m%d_%H%M%S),json=false --exit