"use strict";

const chai = require("chai");
const chaiHttp = require("chai-http");
const { get_token, revoke_token } = require("../authenticate");
const { server } = require("../../src/server");
const { API, BEARER, ROLE } = require("../config");
let should = require('chai').should();
chai.use(chaiHttp);

describe('Should download life tmp agent application file', () => {
    var access_token;

    before((done) => {
        get_token(ROLE.PGL_REV_01).then(token => {
            access_token = token;
            done();
        });
    });

    after(function (done) {
        revoke_token(access_token).then((data) => {
            done();
        });
    });

    it('It should download life tmp agent application file by companyOid and fileName', (done) => {
        chai.request(API.BASE_URL)
            .get(API.CONTEXT + API.AGENT_APPLICATION_DOWNLOAD_PATH + '/pragatilife/02d004f0-4448-4cce-8322-df4dc515f14d.png')
            .set('Authorization', `${BEARER} ${access_token}`)
            .end((err, res) => {
                // res.should.have.status(200);
                done();
            });
    });
});


//NODE_ENV=loc node_modules/mocha/bin/mocha 'test/tmp-agent-application/tmp-agent-application-download-file.js' --timeout 1000 --reporter mochawesome --reporter-options reportDir=test_report,reportFilename=test_report_$(date +%Y%m%d_%H%M%S),json=false --exit