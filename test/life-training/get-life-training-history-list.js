"use strict";

const chai = require("chai");
const chaiHttp = require("chai-http");
const { get_token, revoke_token } = require("../authenticate");
const { server } = require("../../src/server");
const { API, BEARER, ROLE } = require("../config");
let should = require('chai').should();
chai.use(chaiHttp);

describe('Should get Life Training History List', () => {
    var access_token;

    before((done) => {
        get_token(ROLE.PGL_REV_01).then(token => {
            access_token = token;
            done();
        });
    });

    after(function (done) {
        revoke_token(access_token).then((data) => {
            done();
        });
    });

    it('it should get Life Training History List', (done) => {
        chai.request(API.BASE_URL)
            .get(API.CONTEXT + API.AGENT_APPLICATION_LIFE_TRAINING_HIST_GET_PATH + '?offset=0&limit=10&sortColId=createdOn&sortOrder=desc&searchText=&companyOid=pragatilife&filterType=Valid&trainingInstituteOid=')
            .set('Authorization', `${BEARER} ${access_token}`)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('status');
                res.body.should.have.property('code');
                res.body.should.have.property('data').be.a('array');
                res.body.should.have.property('status').eql(true);
                res.body.should.have.property('code').eql(200);
                res.body.should.have.nested.property('data[0].certificateNumber');
                res.body.should.have.nested.property('data[0].traineeName');
                res.body.should.have.nested.property('data[0].trainingName');
                res.body.should.have.nested.property('data[0].trainingInstituteName');
                res.body.should.have.nested.property('data[0].identificationType');
                res.body.should.have.nested.property('data[0].trainingCompletionDate');
                res.body.should.have.nested.property('data[0].identificationNumber');
                res.body.should.have.nested.property('data[0].companyoid');
                res.body.should.have.nested.property('data[0].createdby');
                res.body.should.have.nested.property('data[0].createdon');
                res.body.should.have.nested.property('data[0].licenceissued');
                res.body.should.have.nested.property('data[0].status');
                done();
            });
    });
});

//NODE_ENV=loc node_modules/mocha/bin/mocha 'test/training-institute/get-life-training-history-list.js' --timeout 1000 --reporter mochawesome --reporter-options reportDir=test_report,reportFilename=test_report_$(date +%Y%m%d_%H%M%S),json=false --exit