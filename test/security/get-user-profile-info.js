"use strict";

const chai = require("chai");
const chaiHttp = require("chai-http");
const { get_token, revoke_token } = require("../authenticate");
const { server } = require("../../src/server");
const { API, BEARER, ROLE } = require("../config");
let should = require('chai').should();
chai.use(chaiHttp);

describe('should get user profile', () => {
    var access_token;

    before((done) => {
        get_token(ROLE.PGL_REV_01).then(token => {
            access_token = token;
            done();
        });
    });

    after(function (done) {
        revoke_token(access_token).then((data) => {
            done();
        });
    });

    it('It should get user profile by userid', (done) => {
        chai.request(API.BASE_URL)
            .post(API.CONTEXT + API.USER_PROFILE_INFO)
            .set('Authorization', `${BEARER} ${access_token}`)
            .send()
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('status');
                res.body.should.have.property('code');
                res.body.should.have.property('data').be.a('object');
                res.body.should.have.property('status').eql(true);
                res.body.should.have.property('code').eql(200);
                res.body.should.have.nested.property('data.loginId');
                res.body.should.have.nested.property('data.username');
                res.body.should.have.nested.property('data.lastlogintime');
                res.body.should.have.nested.property('data.resetrequired');
                res.body.should.have.nested.property('data.roleoid');
                res.body.should.have.nested.property('data.accessjson');
                done();
            });
    });
});

//NODE_ENV=loc node_modules/mocha/bin/mocha 'test/security/get-user-profile-info.js' --timeout 1000 --reporter mochawesome --reporter-options reportDir=test_report,reportFilename=test_report_$(date +%Y%m%d_%H%M%S),json=false --exit