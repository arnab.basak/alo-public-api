"use strict";

const chai = require("chai");
const chaiHttp = require("chai-http");
const { get_token, revoke_token } = require("../authenticate");
const { server } = require("../../src/server");
const { API, BEARER, ROLE } = require("../config");
let should = require('chai').should();
chai.use(chaiHttp);

describe('Should provide Login', () => {
    var access_token;

    before((done) => {
        get_token(ROLE.PGL_REV_01).then(token => {
            access_token = token;
            done();
        });
    });

    after(function (done) {
        revoke_token(access_token).then((data) => {
            done();
        });
    });

    it('It should provide Login by an user', (done) => {
        chai.request(API.BASE_URL)
            .post(API.CONTEXT + API.SIGNIN)
            .send({
                "userid": "pgl-rev-01",
                "password": "asdf12"
            })
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('status');
                res.body.should.have.property('code');
                res.body.should.have.property('status').eql(true);
                res.body.should.have.property('code').eql(200);
                res.body.should.have.property('token');
                res.body.should.have.nested.property('token.access_token');
                res.body.should.have.nested.property('token.refresh_token');
                res.body.should.have.nested.property('token.token_type');
                res.body.should.have.nested.property('token.access_token_expire_in');
                res.body.should.have.nested.property('token.refresh_token_expire_in');
                done();
            });
    });
});

//NODE_ENV=loc node_modules/mocha/bin/mocha 'test/security/signin.js' --timeout 1000 --reporter mochawesome --reporter-options reportDir=test_report,reportFilename=test_report_$(date +%Y%m%d_%H%M%S),json=false --exit