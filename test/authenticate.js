"use strict";

const axios = require("axios");
const { API, BEARER } = require("../test/config");

const get_token = async (role) => {
    let { data } = await axios.post(API.BASE_URL + API.CONTEXT + API.SIGNIN, role);
    return data.token.access_token;
};

const revoke_token = async (token) => {
    let _headers = { Authorization: `${BEARER} ${token}` };
    let { data } = await axios.put(API.BASE_URL + API.CONTEXT + API.SIGNOUT, {}, { headers: _headers });
    return data;
};

module.exports = {
    get_token: get_token,
    revoke_token: revoke_token,
}