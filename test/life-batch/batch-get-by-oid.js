"use strict";

const chai = require("chai");
const chaiHttp = require("chai-http");
const { get_token, revoke_token } = require("../authenticate");
const { server } = require("../../src/server");
const { API, BEARER, ROLE } = require("../config");
let should = require('chai').should();
chai.use(chaiHttp);

describe('Should get batch by oid', () => {
    var access_token;

    before((done) => {
        get_token(ROLE.PGL_REV_01).then(token => {
            access_token = token;
            done();
        });
    });

    after(function (done) {
        revoke_token(access_token).then((data) => {
            done();
        });
    });

    it('It should get batch by oid', (done) => {
        chai.request(API.BASE_URL)
            .get(API.CONTEXT + API.AGENT_APPLICATION_BATCH_GET_BY_OID_PATH + '/ddee6c6c-9fb2-4117-8552-a9c5172bac5b')
            .set('Authorization', `${BEARER} ${access_token}`)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('status');
                res.body.should.have.property('code');
                res.body.should.have.property('data').be.a('object');
                res.body.should.have.property('status').eql(true);
                res.body.should.have.property('code').eql(200);
                res.body.should.have.nested.property('data.batchId');
                res.body.should.have.nested.property('data.companyOid');
                res.body.should.have.nested.property('data.companyType');
                res.body.should.have.nested.property('data.coverLetterRefNo');
                res.body.should.have.nested.property('data.idraNoteNumber');
                done();
            });
    });
});

//NODE_ENV=loc node_modules/mocha/bin/mocha 'test/batch/batch-get-by-oid.js' --timeout 1000 --reporter mochawesome --reporter-options reportDir=test_report,reportFilename=test_report_$(date +%Y%m%d_%H%M%S),json=false --exit