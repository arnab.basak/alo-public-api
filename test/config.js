"use strict";

module.exports = {
    BEARER: "Bearer",
    API: {
        BASE_URL: "http://localhost:3000",

        CONTEXT: "/api",

        SIGNIN: "/v1/user/signin",
        SIGNOUT: "/v1/user/signout",

        SEND_SMS: "/v1/sms/send",

        AGENT_APPLICATION_GET_BY_OID: "/get-by-oid/{oid}",
        USER_PROFILE_INFO: "/user/profile/info",

        AGENT_APPLICATION_TRAINING_INS_GET_ALL_PATH: "/v1/agent/application/training/institute/get-all-list",
        AGENT_APPLICATION_LIFE_TRAINING_HIST_GET_PATH: "/v1/agent/application/life/training/history/get-list",

        AGENT_ACCESS_NUMBER_GEN_PATH: "/v1/agent/application/gen-access-number",
        AGENT_ACCESS_NUMBER_VERY_PATH: "/v1/agent/application/verify-access-number",

        AGENT_APPLICATION_LICENCE_GET_RENEWAL_LIST_PATH: "/v1/agent/application/licence/get-renewal-list",
        AGENT_APPLICATION_LICENCE_GET_LIST_PATH: "/v1/agent/application/licence/get-list",
        AGENT_APPLICATION_LICENCE_GET_BY_OID_PATH: "/v1/agent/application/licence/get-by-oid",
        AGENT_APPLICATION_LICENCE_GET_DISTRIBUTE_LIST_PATH: "/v1/agent/application/licence/get-distribute-list",
        AGENT_APPLICATION_LICENCE_DISTRIBUTE_PATH: "/v1/agent/application/licence/distribute",

        AGENT_APPLICATION_TYPE_PATH: "/v1/agent/application/get-type-list",

        AGENT_APPLICATION_BATCH_GET_BY_OID_PATH: "/v1/agent/application/batch/get-by-oid",
        AGENT_APPLICATION_BATCH_RESOURCES: "/v1/agent/application/batch",
        AGENT_APPLICATION_BATCH_GET_ALL_PATH: "/v1/agent/application/batch/get-all-list",
        AGENT_APPLICATION_BATCH_SAVE_PATH: "/v1/agent/application/batch/save",
        AGENT_APPLICATION_BATCH_DISCARD_PATH: "/v1/agent/application/batch/discard",
        AGENT_APPLICATION_BATCH_UPDATE_PATH: "/v1/agent/application/batch/update",
        AGENT_APPLICATION_BATCH_GET_LOCK_PATH: "/v1/agent/application/batch/lock",
        AGENT_APPLICATION_BATCH_GET_UNLOCK_PATH: "/v1/agent/application/batch/unlock",
        AGENT_APPLICATION_BATCH_GET_BY_USER_PATH: "/v1/agent/application/batch/get-list-by-user",
        AGENT_APPLICATION_BATCH_UPDATE_ACK_REF_PATH: "/v1/agent/application/batch/update-ack-reference",
        AGENT_APPLICATION_BATCH_GET_APPROVED_LIST_PATH: "/v1/agent/application/batch/get-approve-list",

        GEO_DATA_DIVISIONS: "/v1/geodata/divisions",
        GEO_DATA_DISTRICTS: "/v1/geodata/districts",
        GEO_DATA_UPAZILLAS: "/v1/geodata/upazillas",
        GEO_DATA_UNIONS: "/v1/geodata/unions",

        AGENT_APPLICATION_LETTER_APPROVED: "/v1/agent/application/employee/letter/Approved",
        AGENT_APPLICATION_LIFE_LETTER_BY_OID_PATH: "/v1/agent/application/employee/letter/get-by-oid",

        AGENT_APPLICATION_TMP_GET_LIST_PATH: "/v1/agent/application/life/1/get-list",
        LIFE_AGENT_APPLICATION_GET_BY_OID: "/v1/agent/application/life/1/get-by-oid",
        AGENT_APPLICATION_TMP_LOCK: "/v1/agent/application/life/1/lock",
        AGENT_APPLICATION_TMP_UNLOCK: "/v1/agent/application/life/1/unlock",
        AGENT_APPLICATION_TMP_DELETE: "/v1/agent/application/life/1/delete",
        AGENT_APPLICATION_TMP_UPDATE: "/v1/agent/application/life/1/update",
        AGENT_APPLICATION_TMP_SAVE: "/v1/agent/application/life/1/save",
        AGENT_APPLICATION_TMP_APPROVE_PATH: "/v1/agent/application/life/1/approve",
        AGENT_APPLICATION_TMP_GET_REGISTRY_LIST_PATH: "/v1/agent/application/life/1/get-registry-list",
        AGENT_APPLICATION_GET_BY_OID_PATH: "/v1/agent/application/life/1/get-agent-appli-by-oid",

        AGENT_APPLICATION_DOWNLOAD_PATH: "/v1/agent/application/download",
        AGENT_APPLICATION_UPLOAD_PATH: "/v1/agent/application/upload",
        AGENT_APPLICATION_DELETE_PATH: "/v1/agent/application/delete",

        AGENT_APPLICATION_LICENCE_LINK_PATH: "/v1/agent/application/life/3/link",
        AGENT_APPLICATION_LICENCE_UNLINK_PATH: "/v1/agent/application/life/3/unlink",
        AGENT_APPLICATION_LICENCE_SAVE_EDIT_PATH: "/v1/agent/application/life/3/save-edit",
        AGENT_APPLICATION_LICENCE_SAVE_PATH: "/v1/agent/application/life/3/save",
        AGENT_APPLICATION_LICENCE_UPDATE_PATH: "/v1/agent/application/life/3/update",
        AGENT_APPLICATION_LICENCE_VERIFY_CODE_PATH: "/v1/agent/application/life/3/verify-agent-code",
        AGENT_APPLICATION_LICENCE_APPROVE_PATH: "/v1/agent/application/life/3/approve",

        AGENT_APPLICATION_RENEW_LICENCE_VERIFY_CODE_PATH: "/v1/agent/application/renew/life/5/verify-agent-code",
        AGENT_APPLICATION_RENEW_LICENCE_SAVE_PATH: "/v1/agent/application/renew/life/5/save",
        AGENT_APPLICATION_RENEW_LICENCE_SAVE_EDIT_PATH: "/v1/agent/application/renew/life/5/save-edit",
        AGENT_APPLICATION_RENEW_LICENCE_APPROVE_PATH: "/v1/agent/application/renew/life/5/approve",

        LIFE_AGENT_APPLICATION_GET_LIST_BY_COMPANY_PATH: "/v1/agent/application/life",
        LIFE_AGENT_APPLICATION_GET_BY_OID_PATH: "/v1/agent/application/life/get-by-oid",
        LIFE_AGENT_APPLICATION_GET_APPROVED_OR_REJECTED_LIST: "/v1/agent/application/life/get-approved-or-rejected-list",

        AGENT_APPLICATION_COMPANY_RESOURCES: "/v1/agent/application/company/get-list",

        AGENT_ACCESS_NUMBER_VERY_PUBLIC_PATH: "/public/v1/agent/application/verify-access-number",
        AGENT_APPLICATION_UPLOAD_PUBLIC_PATH: "/public/v1/agent/application/upload",
        AGENT_APPLICATION_DELETE_PUBLIC_PATH: "/public/v1/agent/application/delete",
        AGENT_APPLICATION_DOWNLOAD_PUBLIC_PATH: "/public/v1/agent/application/download",
        AGENT_APPLICATION_TMP_PUBLIC_SAVE_PATH: "/public/v1/agent/application/life/1/save",

        NON_LIFE_AGENT_APPLICATION_TMP_SAVE: "/v1/agent/application/nonlife/1/save",

    },
    ROLE: {
        PGL_REV_01: { "userid": "pgl-rev-01", "password": "sa" },
        PGL_REV_02: { "userid": "pgl-rev-02", "password": "sa" },
        IDRA_APP_01: { "userid": "idra-app-01", "password": "sa" }
    },
};
