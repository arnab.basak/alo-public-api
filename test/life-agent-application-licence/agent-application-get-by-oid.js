"use strict";

const chai = require("chai");
const chaiHttp = require("chai-http");
const { get_token, revoke_token } = require("../authenticate");
const { server } = require("../../src/server");
const { API, BEARER, ROLE } = require("../config");
let should = require('chai').should();
chai.use(chaiHttp);

describe('Should get agent application', () => {
    var access_token;

    before((done) => {
        get_token(ROLE.PGL_REV_01).then(token => {
            access_token = token;
            done();
        });
    });

    after(function (done) {
        revoke_token(access_token).then((data) => {
            done();
        });
    });

    it('It should get agent application by oid', (done) => {
        chai.request(API.BASE_URL)
            .get(API.CONTEXT + API.LIFE_AGENT_APPLICATION_GET_BY_OID_PATH +'/a5929a76-2834-4989-9100-89d1dda1e79c')
            .set('Authorization', `${BEARER} ${access_token}`)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('status');
                res.body.should.have.property('code');
                res.body.should.have.property('data').be.a('object');
                res.body.should.have.property('status').eql(true);
                res.body.should.have.property('code').eql(200);
                res.body.should.have.nested.property('data.agentCode');
                res.body.should.have.nested.property('data.applicationId');
                res.body.should.have.nested.property('data.eduCertificatePath');
                res.body.should.have.nested.property('data.nidNumber');
                res.body.should.have.nested.property('data.trainingInfo');
                done();
            });
    });
});

//NODE_ENV=loc node_modules/mocha/bin/mocha 'test/agent-application-licence/agent-application-get-by-oid.js' --timeout 1000 --reporter mochawesome --reporter-options reportDir=test_report,reportFilename=test_report_$(date +%Y%m%d_%H%M%S),json=false --exit