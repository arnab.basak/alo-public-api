# 01-API

### To generate password at cmd prompt

```
node -e "console.log(require('bcryptjs').hashSync('asdf1234', 10));"
```
> sa = $2a$10$boGWhTFmdmUKz9wXJJE88.XOsZkN8F9aSgXVlY8YIO.bXckfyb6Ni

# API
- Node Js


<br>

# Database
- Postgresql

<br>

# API Testing
- Mocha Js
- Chai Js - Assertion Library
- Mochawesome - Testing Report Library
